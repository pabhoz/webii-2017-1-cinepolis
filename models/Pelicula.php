<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author Pabhoz
 */
class Pelicula extends Model {

  protected static $table = "Pelicula";

  private $id;
  private $titulo;
  private $img;

  function __construct($id, $titulo, $img) {
      $this->id = $id;
      $this->titulo = $titulo;
      $this->img = $img;
  }

  
  public function getMyVars(){
      return get_object_vars($this);
  }
  
  static function getTable() {
      return self::$table;
  }

  function getId() {
      return $this->id;
  }

  function getTitulo() {
      return $this->titulo;
  }

  function getImg() {
      return $this->img;
  }

  static function setTable($table) {
      self::$table = $table;
  }

  function setId($id) {
      $this->id = $id;
  }

  function setTitulo($titulo) {
      $this->titulo = $titulo;
  }

  function setImg($img) {
      $this->img = $img;
  }



}
